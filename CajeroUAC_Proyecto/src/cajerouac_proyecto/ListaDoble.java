package cajerouac_proyecto;

public class ListaDoble {
    private NodoDoble inicio, fin;
    public ListaDoble(){
        inicio=fin=null;
    }
    public boolean estaVacia(){
        return inicio == null;
    }
    public void agregarAlFinal(int elemento){
        if (!estaVacia()){
            fin= new NodoDoble(elemento, null, fin);
            fin.anterior.siguiente = fin;
        }else{
            inicio=fin=new NodoDoble(elemento);
        }
    }
    public void agregarAlInicio(int elemento){
        if (!estaVacia()){
            inicio= new NodoDoble(elemento, inicio, null);
            inicio.siguiente.anterior = inicio;
        }else{
            inicio=fin=new NodoDoble(elemento);
        }
    }
    public void mostrarListaInicioFin(){
        if (!estaVacia()){
            String conector = "<=>";
            String datos = "<-";
            NodoDoble auxiliar = inicio;
            while(auxiliar !=null){
               datos = datos +"["+auxiliar.dato+"] "+conector;
               auxiliar = auxiliar.siguiente;
            }
            datos += "->"; 
            System.out.println(datos);
        }
    }
    public void mostrarListaFinInicio(){
        if (!estaVacia()){
            String conector= "<=>";
            String datos = "<-";
            NodoDoble auxiliar = fin;
            while(auxiliar !=null){
               datos = datos +"["+auxiliar.dato+"]"+conector;
               auxiliar = auxiliar.anterior;
            }
            datos += "->"; 
            System.out.println(datos);
        }
    } 
    public int eliminarDelInicio(){
        int elemento = inicio.dato;
        if (inicio == fin){
            inicio=fin=null;
        }else{
            inicio = inicio.siguiente;
            inicio.anterior= null;
        }
        return elemento;
    }
    
    public int eliminarDelFinal(){
        int elemento = fin.dato;
        if (inicio == fin){
            inicio=fin=null;
        }else{
            fin = fin.anterior;
            fin.siguiente= null;
        }
        return elemento;
    }

    
}